ARG RAILS_IMAGE=

# Use a composite image to set the permissions properly in the artifacts. This
# is needed due to two limitations with Docker:
# 1. ADD doesn't set permissions for tarballs: https://github.com/moby/moby/issues/35525
# 2. docker cp doesn't preserve UID/GID: https://github.com/moby/moby/issues/41727
FROM ${RAILS_IMAGE} as composite

ARG UID=1000

ADD gitlab-rails-bootsnap-ee.tar.gz /assets
RUN chown -R ${UID}:0 /assets/srv/gitlab

## FINAL IMAGE ##

ARG RAILS_IMAGE

FROM ${RAILS_IMAGE}

ARG GITLAB_VERSION
ARG GITLAB_USER=git
ARG UID=1000
ARG DNF_OPTS
ENV LIBDIR ${LIBDIR:-"/usr/lib64"}

LABEL source="https://gitlab.com/gitlab-org/build/CNG/-/tree/master/gitlab-toolbox" \
      name="GitLab Toolbox" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${GITLAB_VERSION} \
      release=${GITLAB_VERSION} \
      summary="Toolbox is an entry point for interaction with other containers in the cluster." \
      description="Toolbox is an entry point for interaction with other containers in the cluster. It contains scripts for running Rake tasks, backup, restore, and tools to interact with object storage."

ADD gitlab-toolbox-ee.tar.gz /
ADD gitlab-python.tar.gz /

COPY scripts/bin/* /usr/local/bin/
COPY scripts/lib/* ${LIBDIR}/ruby/vendor_ruby/
COPY --from=composite /assets /

RUN microdnf ${DNF_OPTS} install --best --assumeyes --nodocs --setopt=install_weak_deps=0 ca-certificates openssl \
    && microdnf clean all

RUN chown -R ${UID}:0 /scripts /home/${GITLAB_USER} /var/log/gitlab && \
    chmod -R g=u /scripts /home/${GITLAB_USER} /var/log/gitlab

# Declare /var/log volume after initial log files
# are written to the perms can be fixed
VOLUME /var/log

USER ${UID}

ENTRYPOINT ["/usr/local/bin/entrypoint-ubi8.sh"]
